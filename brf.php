<?php 

error_reporting(E_ALL ^ E_DEPRECATED);
ini_set('display_errors','On');

set_include_path(get_include_path() . PATH_SEPARATOR . realpath("../includes/"));

require_once("configure.php"); // This is the function file that the that the system use.

$objEgrannar = new EgrannarImpl();

$strMessage = "";

if(isset($_GET["remove"])){
	
	$objBrf = new clsBrf($_GET["remove"]);
	
	if($objBrf->intId > 0){
		$objEgrannar->removeBrf($objBrf->intId);
		$strMessage = $_GET["remove"]." removed";		
	}else{
		$strMessage = "could not find:" . $_GET["remove"];
	}
}

?>


<h1>Egrannar admin brf</h1>
<h2><?php echo $strMessage;?></h2>
<ul>
	<?php foreach (clsBrf::getList("","","","") as $brf):?>
		<li><a href="javascript:areYouSure('<?php echo $_SERVER["PHP_SELF"]."?page=brf&remove=".$brf->intId;?>','Är du Säker?');"><?php echo $brf->strName;?></a></li>
	<?php endforeach;?>
</ul>
