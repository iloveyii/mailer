<?php
    mb_internal_encoding('utf-8');
	
    /**
	 * @property WebDriver $driver
	 */
	class DataCommand extends CConsoleCommand {
        
        /**
         * Performs data extraction
         * 
         * @param string $ad down : download all, newad: download only new
         * @param string $county_id all : download all counties, numeric: download only given
         */
		public function actionIndex() {
           $this->import();
		}

        public function import() {
            echo 'Importing ... ' . PHP_EOL;
            // check log if already imported

            $importDir = Yii::getPathOfAlias('Images.deltaco');
            $importFiles = scandir($importDir);
            foreach ($importFiles as $importFileName) {
                if(!in_array($importFileName, array('.', '..'))) { // remove . and ..
                    // find if already imported
                    // $Importlog = Importlog::model()->findByAttributes(array('fnimported'=>$importFileName));
                    $Importlog = Importlog::model()->findByAttributes(array('fnimported'=>$importFileName));
                    // if(isset($Importlog)) {
                    if(isset($Importlog) && ($Importlog->imported == 'yes')) {
                        echo "The file $importFileName already imported " . PHP_EOL;
                    } else {
                        $Importlog = new Importlog();
                        $Importlog->fnimported = $importFileName;
                        $Importlog->imported = 'yes';
                        $Importlog->save(false);
                        
                        if($this->doImport($importFileName)) {
                            $Importlog->updated = date('Y-m-d H:i:s');
                            $Importlog->save(false);
                        }
                    }
                }
            }

        }
    
        public function doImport($importFile) {

            $importFile = Yii::getPathOfAlias('Images.deltaco')."/{$importFile}";

            if(!file_exists($importFile)) {
                echo  '<span class="alert-danger">Thumb '.$importFile.' does not exist on disk</span>';
            }  else {
                $file = fopen($importFile,"r");
                while (!feof($file)) {
                    $arrImport = fgetcsv($file,0,';');
                    // print_r($arrImport); //  [0] => product_no [1] => name [2] => quantity 
                    // #1: Check if quantity

                    $this->checkChanges($arrImport);
                    // #2: Check if old
                    // #3: Check if new

                    // print($arrImport[0]) . '' . PHP_EOL;
                }
                fclose($file);

                return TRUE;
            }
        }
    
        private function checkChanges($arrImport) {
            $product_no = $arrImport[0];
            $quantity = $arrImport[2];
            if(empty($product_no))
                return;

            $keys = array('product_no','name','quantity','price','barcode','company_name','image_name','weight','main_category','sub_category','type','model','description');

            $model = Product::model()->findByAttributes(array('product_no'=>$product_no));
            if(isset($model)) {
                echo '.';
                $model->status = Product::OLDPRODUCT;
                if(abs($quantity - $model->quantity) > 5) {
                    $model->status = Product::QTYCHANGEDPRODUCT;
                    $model->updated = date('Y-m-d H:i:s');
                }
            } else {
                echo '+';
                $model = new Product();
                // get associate array
                $Product = array_combine($keys, $arrImport);
                $model->attributes = $Product;
                $model->sello='Nej';
                $model->selling='Ja';
                if($model->price > 8000) $model->selling = 'Nej';
                $model->saleprice= (1.50 * $model->price) + 10;
                if($model->weight < 1) $model->weight= $model->weight * 1000;
                $model->status = Product::NEWPRODUCT;
                $model->updated = date('Y-m-d H:i:s');
            }

            return $model->save(FALSE);
        }
	}