<?php
/**
 *  Extending CLinkPager so that we can give it a custom css file for UL
 */

class LinkPager extends CLinkPager
{
    const CSS_DISABLED_PAGE='disabled';
    
    public function __construct() {
        $this->htmlOptions['class']='pagination';
        $this->header='';
        $this->selectedPageCssClass='active';
        $this->prevPageLabel='&laquo;';
        $this->nextPageLabel='&raquo;';
        $this->firstPageLabel = 'Första';
        $this->lastPageLabel = 'Sista';
        $this->hiddenPageCssClass= self::CSS_DISABLED_PAGE;
        $this->maxButtonCount=5;
    }
    
}