 <!-- Navigation bar with drop down box
================================================== -->
<nav style="margin-bottom:0;" role="navigation" class="navbar navbar-default navbar-static-top logga eg">
    <div class="navbar-header col-md-2 col-sm-5 col-xs-5">
        <div class="navbar-header pull-right hidden-lg">
            <a id="nav-expander" class="nav-expander">
              MENU &nbsp;<i class="fa fa-bars fa-lg white"></i>
            </a>
        </div>
        <a style="padding:10px 15px;display:table;" href="<?php echo Yii::app()->getHomeUrl();?>" class="navbar-brand"><img style="height: 30px;" src="<?php echo Yii::app()->baseUrl; ?>/images/logga.png" alt="Admin" /></a>
    </div>
    <!-- /.navbar-header -->

    <div class="col-md-1 col-sm-3 col-xs-7 pull-right">
        <ul class="nav navbar-top-links navbar-right pull-right menu-top-eg" style="line-height:28px; margin-right: 10px">
            <li class="dropdown">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                    <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <?php if(! Yii::app()->user->isGuest) :?>
                    <li><a href="<?php echo Yii::app()->createUrl('email/create'); ?>"><i class="fa fa-eye fa-fw"></i> Send Email</a></li>
                    <li class="divider"></li>
                    <?php endif;?>
                    <?php 
                    if(Yii::app()->user->isGuest){
                        $url = Yii::app()->createUrl('site/login');
                        $text = 'Login';
                        $class= 'fa fa-sign-in fa-fw';
                    } else {
                        $url = Yii::app()->createUrl('site/logout');
                        $text = 'Logout';
                        $class= 'fa fa-sign-out fa-fw';
                    }  ?>
                    <li><a href="<?php echo $url; ?>"><i class="<?php echo $class; ?>"></i><?php echo $text; ?></a></li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->
    </div>

</nav>
<!-- Navigation -->
