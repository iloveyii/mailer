<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
Yii::setPathOfAlias('Images',dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR.'images');
Yii::setPathOfAlias('root', realpath('./'));

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Console Application',

	// preloading 'log' component
	'preload'=>array('log'),
    'aliases' => array(
		'application' => 'root.protected',
	),
    'import' => array(
		'root.models.*',
		'root.components.*',
		'root.extensions.*',
	),
    
	// application components
	'components'=>array(
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=shop24',
			'emulatePrepare' => true,
			'username' => 'shop24',
			'password' => 'letshopnow',
//			'username' => 'root',
//			'password' => 'root',
			'charset' => 'utf8',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
	),
);