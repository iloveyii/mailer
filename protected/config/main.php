<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
defined('DS') or define('DS',DIRECTORY_SEPARATOR);

Yii::setPathOfAlias('Images',dirname(dirname(dirname(__FILE__))).DS.'images');

//Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
$strDbHost= $_SERVER["DB1_HOST"];
$strDbPort= $_SERVER["DB1_PORT"];
$strDbPassword= $_SERVER["DB1_PASS"];
$strDbUser= $_SERVER["DB1_USER"];
$strDbName= $_SERVER["DB1_NAME"];

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Egrannar Admin',
    'defaultController' => 'email',
	// preloading 'log' component
	'preload'=>array('log','bootstrap'),
//    'theme'=>'bootstrap', 
    // autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'gii',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
            
		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
                'email/send'=>'email/create',
                'resetpassword/<hash:\w+>'=>'site/resetpassword',
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
            'showScriptName' => false,
		),
        /*    
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
         */
       
		// uncomment the following to use a MySQL database
		'db'=>array(
			'emulatePrepare' => true,
			'charset' => 'utf8',
//            'connectionString' => 'mysql:host=localhost;dbname=mailer',
//			'username' => 'root',
//			'password' => 'root',
            'connectionString' => 'mysql:host='.$strDbHost.';port='.$strDbPort.';dbname='.$strDbName,
			'username' => $strDbUser,
			'password' => $strDbPassword,
		),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                    array(
                            'class'=>'CFileLogRoute',
                            'levels'=>'error, warning',
                    ),
                    // uncomment the following to show log messages on web pages
                    /*
                    array(
                            'class'=>'CWebLogRoute',
                    ), */
            ),
		),
            
        'bootstrap' => array(
		    'class' => 'ext.bootstrap.components.Bootstrap',
		    'responsiveCss' => FALSE,
            'coreCss' => FALSE,
            'yiiCss' => FALSE,
            'enableJS' => TRUE,
		), 
        'clientScript' => array(
            'scriptMap' => array(
                'bootstrap.min.js' => '/js/bootstrap.js',
                // 'jquery.js' => '/js/jquery-1.11.0.js',
            ),
        ),
        // for resing images 
        'imageResizer'=>array(
            'class'=>'ImageResizer'
        ),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);