<?php

class SiteController extends Controller
{
    public $layout = 'main';
    
    /**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionDashboard()
	{
        if(Yii::app()->user->isGuest) {
            $this->redirect($this->createUrl('site/login'));
            Yii::app()->end();
        }
        $this->layout='main';
        // # select winner_id, winnerCount, loser_id, ifnull(loserCount, 0) from
        $sql = "SELECT mem_id as id, ifnull(totalPlayed, 0) as totalPlayed, namn from
                (
                    SELECT winner_id as id, sum(winnerCount + ifnull(loserCount, 0)) as totalPlayed from 
                                (
                                    SELECT winner_id, count(id) as winnerCount from `match` 
                                    GROUP BY winner_id
                                ) t1 

                            LEFT JOIN (
                                    SELECT loser_id,count(id) as loserCount from `match` 
                                    GROUP BY loser_id
                                ) t2
                            ON t1.winner_id=t2.loser_id
                            GROUP BY id

                ) t3
                RIGHT JOIN member 
                ON t3.id=member.mem_id
                 
        ";
        
        $topActiveOrderBy = "ORDER BY t3.totalPlayed DESC LIMIT 10";
        $leastActiveOrderBy = "ORDER BY t3.totalPlayed ASC LIMIT 10";
        
        $topActive = Match::model()->findAllBySql($sql.$topActiveOrderBy);
        // array data provider
        $rawData = array();
        foreach ($topActive as $match) {
           $rawData[]= array(
               'id'=> $match->id,
               'totalPlayed'=>$match->totalPlayed,
               'namn'=>$match->namn 
            );
        }
        $arrayDataProviderTopActive=new CArrayDataProvider($rawData, array(
            'pagination'=>FALSE
        ));
        
        $leastActive = Match::model()->findAllBySql($sql.$leastActiveOrderBy);
        // array data provider
        $rawData = array();
        foreach ($leastActive as $match) {
           $rawData[]= array(
               'id'=> $match->id,
               'totalPlayed'=>$match->totalPlayed,
               'namn'=>$match->namn 
            );
        }
        $arrayDataProviderLeastActive=new CArrayDataProvider($rawData, array(
            'pagination'=>FALSE
        ));
         
        $sqlTopWinners="SELECT mem_id as id, ifnull(totalWon, 0) as totalWon, namn from
                        (
                            SELECT winner_id, count(id) as totalWon from `match` 
                            GROUP BY winner_id         
                        ) t1
                        RIGHT JOIN member 
                        ON t1. winner_id=member.mem_id
                    ORDER BY totalWon DESC
                    LIMIT 10";
        $topWinners = Match::model()->findAllBySql($sqlTopWinners);
        
        
        // summary
        $summary = $this->getSummaryMatches();
        $rawData = array();
        // for array data provider
        foreach ($summary as $label=>$value) {
            $rawData[] =  array('id'=>$label, 'value'=>$value);
        }
        
        $arrayDataProvider=new CArrayDataProvider($rawData, array(
            'pagination'=>FALSE
        ));
        
		$this->render('dashboard', array(
            'topActive'=>$topActive,
            'leastActive'=>$leastActive,
            'topWinners'=>$topWinners,
            'arrayDataProvider'=> $arrayDataProvider,
            'arrayDataProviderLeastActive'=> $arrayDataProviderLeastActive,
            'arrayDataProviderTopActive'=> $arrayDataProviderTopActive,
        ));
	}

    private function getSummaryMatches() {
        // summary
        $summary = array();
        $criteria = new CDbCriteria();
        
        // today
        $startDate = date('Y-m-d');
        $endDate = date('Y-m-d');
        $criteria->condition="date between '{$startDate}' AND '{$endDate}'";
        $countToday = Match::model()->count($criteria);
        $summary['Today']=$countToday;
        
        // this week
        $startDate = $this->getWeekStartDate();
        $endDate = date('Y-m-d', strtotime('+6 days', strtotime($startDate)));
        $criteria->condition="date between '{$startDate}' AND '{$endDate}'";
        $countWeek = Match::model()->count($criteria);
        $summary['This Week']=$countWeek;
        
        // this month
        $startDate = $this->getMonthStartDate();
        $endDate = $this->getMonthEndDate();
        $criteria->condition="date between '{$startDate}' AND '{$endDate}'";
        $countMonth = Match::model()->count($criteria);
        $summary['This Month']=$countMonth;
        
        // prev month
        $currentDate = date('Y-m-d');
        $dateString = "$currentDate first day of previous month";
        $Date=date_create($dateString); 
        $startDate = $Date->format('Y-m-d');
        $dateString = "$currentDate last day of previous month";
        $Date=date_create($dateString); 
        $endDate = $Date->format('Y-m-d');
        
        $criteria->condition="date between '{$startDate}' AND '{$endDate}'";
        $countPrevMonth = Match::model()->count($criteria);
        $summary['Previous Month']=$countPrevMonth;
        
        // this year
        $dateString = "first day of January this year";
        $Date=date_create($dateString); 
        $startDate = $Date->format('Y-m-d');
        $dateString = "last day of December this year";
        $Date=date_create($dateString); 
        $endDate = $Date->format('Y-m-d');
        
        $criteria->condition="date between '{$startDate}' AND '{$endDate}'";
        $countYear = Match::model()->count($criteria);
        $summary['This Year']=$countYear;
        
        // all time
        $criteria = new CDbCriteria();
        $countAllTime = Match::model()->count($criteria);
        $summary['All']=$countAllTime;
        
        return $summary;
    }
    private function getMonthStartDate($format='Y-m-d', $date=false) {
        // set time zone
        date_default_timezone_set('Europe/Stockholm');
        // if date is not given
        if($date ===FALSE) {
            $date = date('Y-m-d'); // current date
        }
        
        $month = date('m', strtotime($date));
        $year = date('o', strtotime($date));
        
        $monthStartDate = date ($format, strtotime("{$year}-{$month}-1"));
        return $monthStartDate;
    }
    
    private function getMonthEndDate($format='Y-m-d', $date=false) {
        // set time zone
        date_default_timezone_set('Europe/Stockholm');
        // if date is not given
        if($date ===FALSE) {
            $date = date('Y-m-d'); // current date
        }
        
        $month = date('m', strtotime($date));
        $year = date('o', strtotime($date));
        $noOfDaysInMonth = date('t', strtotime($date));
        
        $monthEndDate = date ($format, strtotime("{$year}-{$month}-{$noOfDaysInMonth}"));
        return $monthEndDate;
    }
    
    private function getWeekStartDate($format='Y-m-d', $date=false) {
        // set time zone
        date_default_timezone_set('Europe/Stockholm');
        // if date is not given
        if($date ===FALSE) {
            $date = date('Y-m-d'); // current date
        }
        
        $week = date('W', strtotime($date));
        $year = date('o', strtotime($date));
        
        $week = sprintf("%02s", $week);
        $weekStartDate = date ($format, strtotime("{$year}-W{$week}-1"));
        return $weekStartDate;
    }
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
        $this->layout='main';
		$model=new LoginForm;
        
		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->homeUrl);
				// $this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

    public function actionForgotpassword() {
        $model=new ForgotpasswordForm;
        if(isset($_POST['ForgotpasswordForm']))
		{
			$model->attributes=$_POST['ForgotpasswordForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->sendPasswordResetLink()) {
                $email=$model->email;
                $model->unsetAttributes();
                Yii::app()->user->setFlash('success', "<strong> An email containg password reset link has been sent to $email. </strong>");
                $message="<p>Go to your email and click the link and the follow the instructions.</p>"; 
            }
		}
		// display the login form
		$this->render('forgotpassword',array(
            'model'=>$model
        ));
    }
	
    public function actionResetpassword($hash) {
        $model = new Resetpassword('resetpassword');
        // check if hash is valid
        $msg = $model->validHash($hash);
        if($msg === TRUE) {
            
            if(isset($_POST['Resetpassword']))
            {
                $model->attributes=$_POST['Resetpassword'];
                // validate user input and redirect to the previous page if valid
                if($model->validate()) {
                    $msg = $model->resetPass($hash);
                    if($msg===TRUE) {
                        $model->password=''; $model->confirm_password='';
                        Yii::app()->user->setFlash('success', "<strong> Password reset successfully. </strong>");
                    } else {
                        Yii::app()->user->setFlash('success', "<strong> $msg </strong>");
                    }
                }
            }
            
        }  else {
            Yii::app()->user->setFlash('success', "<strong> $msg </strong>");
            // display the login form
            $this->actionForgotpassword();
            return;
        }
            
		// display the login form
		$this->render('resetpassword',array(
            'model'=>$model
        ));
    }
    
	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
    
    public function actionGallery()
	{
        $model = new Member;
		$this->render('gallery', array(
            'model'=>$model
        ));
	}
}