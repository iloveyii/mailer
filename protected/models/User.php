<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property string $id
 * @property string $username
 * @property integer $noEmail
 * @property string $firstname
 * @property string $lastname
 * @property string $password
 * @property string $personnr
 * @property string $phone
 * @property string $mobil
 * @property string $address1
 * @property string $address2
 * @property string $zip
 * @property string $city
 * @property string $country
 * @property string $avatar
 * @property string $avatarImageId
 * @property string $profileTitle
 * @property string $profileText
 * @property string $dateCreated
 * @property string $dateEdited
 * @property string $lastLogin
 * @property string $dateActivated
 * @property string $activationKey
 * @property integer $active
 * @property integer $privacy
 * @property integer $emailErrorType
 * @property string $emailErrorStatus
 * @property string $emailErrorDescription
 * @property string $emailErrorDate
 */
class User extends CActiveRecord
{
    /**
     *
     * @var integer The BRF ID for which the user is admin
     */
    public $adminBrfId;
    
    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('noEmail, active, privacy, emailErrorType', 'numerical', 'integerOnly'=>true),
			array('username, firstname, lastname, password, personnr, phone, mobil, address1, address2, zip, city, country, avatar, profileTitle, activationKey, emailErrorDescription', 'length', 'max'=>255),
			array('avatarImageId', 'length', 'max'=>20),
			array('emailErrorStatus', 'length', 'max'=>50),
			array('profileText, dateCreated, dateEdited, lastLogin, dateActivated, emailErrorDate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, username, noEmail, firstname, lastname, password, personnr, phone, mobil, address1, address2, zip, city, country, avatar, avatarImageId, profileTitle, profileText, dateCreated, dateEdited, lastLogin, dateActivated, activationKey, active, privacy, emailErrorType, emailErrorStatus, emailErrorDescription, emailErrorDate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Användarnamn',
			'noEmail' => 'No Email',
			'firstname' => 'Förnamn',
			'lastname' => 'Efternamn',
			'password' => 'Password',
			'personnr' => 'Personnr',
			'phone' => 'Phone',
			'mobil' => 'Mobil',
			'address1' => 'Address1',
			'address2' => 'Address2',
			'zip' => 'Zip',
			'city' => 'City',
			'country' => 'Country',
			'avatar' => 'Avatar',
			'avatarImageId' => 'Avatar Image',
			'profileTitle' => 'Profile Title',
			'profileText' => 'Profile Text',
			'dateCreated' => 'Date Created',
			'dateEdited' => 'Date Edited',
			'lastLogin' => 'Last Login',
			'dateActivated' => 'Date Activated',
			'activationKey' => 'Activation Key',
			'active' => 'Active',
			'privacy' => 'Privacy',
			'emailErrorType' => 'Email Error Type',
			'emailErrorStatus' => 'Email Error Status',
			'emailErrorDescription' => 'Email Error Description',
			'emailErrorDate' => 'Email Error Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('noEmail',$this->noEmail);
		$criteria->compare('firstname',$this->firstname,true);
		$criteria->compare('lastname',$this->lastname,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('personnr',$this->personnr,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('mobil',$this->mobil,true);
		$criteria->compare('address1',$this->address1,true);
		$criteria->compare('address2',$this->address2,true);
		$criteria->compare('zip',$this->zip,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('avatar',$this->avatar,true);
		$criteria->compare('avatarImageId',$this->avatarImageId,true);
		$criteria->compare('profileTitle',$this->profileTitle,true);
		$criteria->compare('profileText',$this->profileText,true);
		$criteria->compare('dateCreated',$this->dateCreated,true);
		$criteria->compare('dateEdited',$this->dateEdited,true);
		$criteria->compare('lastLogin',$this->lastLogin,true);
		$criteria->compare('dateActivated',$this->dateActivated,true);
		$criteria->compare('activationKey',$this->activationKey,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('privacy',$this->privacy);
		$criteria->compare('emailErrorType',$this->emailErrorType);
		$criteria->compare('emailErrorStatus',$this->emailErrorStatus,true);
		$criteria->compare('emailErrorDescription',$this->emailErrorDescription,true);
		$criteria->compare('emailErrorDate',$this->emailErrorDate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
    /**
     * Returns lastLogin for given user id 
     * @param integer $id id of the user
     */
    public function getLastLogin($id) {
        $model = User::model()->findByPk();
        if(isset($model)) {
            return $model->lastLogin;
        }
        
        return 'NA';
    }
    
    /**
     * Returns the button html with brfId_userId as html id of the button
     * @param int $id BRF id
     */
    public function getAddAdminButton($brfId) {
        $htmlId = $brfId.'_'.$this->id;
        if($this->isEgAdminForBrfId($brfId)) {
            echo '<button id="'.$htmlId.'" class="toggleEgAdmin btn btn-md btn-danger"><i class="fa fa-remove"></i> Ta bort</button>';
        } else {
            echo '<button id="'.$htmlId.'" class="toggleEgAdmin btn btn-md btn-success"><i class="fa fa-plus-circle"></i> Lägga</button>';
        }
    }
    
    public function isEgAdminForBrfId($brfId) {
        $BrfUser = BrfUser::model()->findByAttributes(array('userId'=>$this->id, 'brfId'=>$brfId));
        if(isset($BrfUser)) { // user is already admin
            return TRUE;
        } else { // add as admin
            return FALSE;
        }
        
    }
}
