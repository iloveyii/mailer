<?php
/* @var $this BrfController */
/* @var $model Brf */
/* @var $form CActiveForm */
?>

<div>
    
<?php $form=$this->beginWidget('bootstrap.widgets.FlatActiveForm', array(
	'id'=>'brf-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
    <div class="row">
        <div class="col-md-12">
            <p class="note">Fields with <span class="required">*</span> are required.</p>

            <?php echo $form->errorSummary($model); ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <?php echo $form->labelEx($model,'to'); ?>
                <?php echo $form->textField($model,'to',array('iconClass'=>'fa fa-globe')); ?>
                <?php echo $form->error($model,'to'); ?>
            </div>

            <div class="form-group">
                <?php echo $form->labelEx($model,'subject'); ?>
                <?php echo $form->textField($model,'subject',array('iconClass'=>'fa fa-globe')); ?>
                <?php echo $form->error($model,'subject'); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10">
            <div class="form-group">
                <?php echo $form->labelEx($model,'body'); ?>
                <?php echo $form->textArea($model,'body', array('rows'=>16,'class'=>'form-control')); ?>
                <?php echo $form->error($model,'body'); ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <hr />
            <div class="form-group">
                <button class="btn btn-success btn-lg" type="submit"><i class="fa fa-envelope-o"></i> <?php echo $model->isNewRecord ? 'Send' : 'Save'; ?></button>
            </div>
        </div>
    </div>
<?php $this->endWidget(); ?>
   
</div><!-- row -->