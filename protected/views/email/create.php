<?php
/* @var $this EmailController */
/* @var $model Email */

$this->breadcrumbs=array(
	'Emails'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Email', 'url'=>array('index')),
	array('label'=>'Manage Email', 'url'=>array('admin')),
);
?>

<div class="row">
    <div class="col-md-12">
        <h1>Send Email</h1>
    </div>
</div>


<?php $this->renderPartial('_form', array('model'=>$model)); ?>