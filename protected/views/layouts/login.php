<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Email - Admin </title>
        <meta name="viewport" content="width=device-width">
        <link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/bootstrap.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/main.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/index.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/sidebar.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/font-awesome.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/index.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/offcanvas.css" />
        
    </head>
    <body>
        
        <?php $this->widget('Menu'); ?>
        
        <!-- wrapper 
        ============= -->
        <div id="page-wrapper" style="margin:0px;">
            
            <div class="row">
                <br />
                <div class="col-lg-12">
                      
                    <?php echo $content; ?>
                    
                </div><!-- /.col-lg-12 -->
            </div><!-- /.row -->
            <br /> <br />
        </div>

        <!-- Footer and Modal -->
        <div style="padding-left: 0px; border-top: 1px solid #E7E7E7;">
            <div class="col-md-12">
                <br />
                <div class="col-md-1">
                    <!-- Modal -->
                    <?php $this->widget('AjaxModal'); ?>
                    <!-- Modal -->
                </div>
                <div class="col-md-11">
                    <div class="pull-right">
                        <p>Copyright &COPY; 
                            Shop24
                        </p>
                    </div>
                    
                </div>
                
            </div>
        </div>
        
    <script src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.metisMenu.js"></script>
    <script src="<?php echo Yii::app()->baseUrl; ?>/js/admin.js"></script>
    <script src="<?php echo Yii::app()->baseUrl; ?>/js/offcanvas.js"></script>

    </body>
</html>