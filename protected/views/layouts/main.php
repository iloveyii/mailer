<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Email - Admin </title>
        <meta name="viewport" content="width=device-width">
        <link href="<?php echo Yii::app()->baseUrl; ?>/images/favicon.ico" rel="shortcut icon">
        <link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/bootstrap.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/main.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/index.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/font-awesome.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/style.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/flat.css" />
    </head>
    <body>
        
        <!-- wrapper 
        ============= -->
        <div class="page-wrap">
            <?php $this->widget('Menu'); ?>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12" style="background-color: #fff;">
                        <br>
                        <?php echo $content; ?>
                        <br class="clear33">
                        <br>
                    </div>
                </div>
            </div>
            
        </div>

        <!-- Footer and Modal -->
        <div class="site-footer">
            <div class="col-md-12" style="height: 54px; padding-top: 16px;padding-bottom: 16px; border-bottom: 3px solid green;">
                <div class="col-md-1">
                    <!-- Modal -->
                    <?php $this->widget('AjaxModal'); ?>
                    <!-- Modal -->
                </div>
                <div class="col-md-11">
                    <div class="pull-right">
                        <span>Copyright &COPY; 
                            SoftHem
                        </span>
                    </div>
                    
                </div>
                
            </div>
        </div>
        
    <script>
        /* jQuery to activate off canvas sidebar */
        $(document).ready(function() {
          $('#collapse-left').click(function() {
            /* $('#sidebar').toggleClass('active');*/
            $('#page-wrapper').animate({'margin-left': "30px"});
            $('#sidebar').animate({'margin-left': "-220px"});
          });
          
          $('#collapse-right').click(function() {
            /* $('#sidebar').toggleClass('active');*/
            $('#page-wrapper').animate({'margin-left': "250px"});
            $('#sidebar').animate({'margin-left': "0px"});
          });
          
          /* prevent post of form on Enter key */
          $('#member-search-form').bind("keyup keypress", function(e) {
            var code = e.keyCode || e.which; 
            if (code  == 13) {               
              e.defaultPrevented();
              return false;
            }
          });
          
          /* reset modal content */
          $('#terms').on('hidden.bs.modal', function (e) {
                var thumbImage= "<img  height='80' width='120' src='<?php echo Yii::app()->baseUrl . '/images/loading-bar.gif'; ?>' alt='...' />Namn";
                $('#playerImage').html(thumbImage);
                var thumbMatches= "<img width='200' src='<?php echo Yii::app()->baseUrl . '/images/loading.gif'; ?>' alt='Loading...' />Lista över matcher";
                $('#playerMatches').html(thumbMatches);
          });
          
        });
    </script>    
    
    <script src="<?php echo Yii::app()->baseUrl; ?>/js/admin.js"></script>
    <script src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.navgoco.js"></script>
    <script src="<?php echo Yii::app()->baseUrl; ?>/js/main.js"></script>

    </body>
</html>