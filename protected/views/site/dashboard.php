<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<h1><center>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i></center></h1>
<hr />
<div class="row">
    <div class="col-md-12">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-green">
                <div class="panel-heading">
                    Top Winners
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="flot-chart">
                        <div class="flot-chart-content" id="flot-pie-chart"></div>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
    </div>
</div>
<hr />

<div class="row">
    <div class="col-md-4">
        <h2>Most Active Players</h2>
        <?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
                'id'=>'stats-grid',
                'dataProvider'=>$arrayDataProviderTopActive,
                'type'=>'striped bordered',
                'hideHeader'=>true,
                'template'=>'{items}',
                'columns'=>array(
                    'id',
                    'totalPlayed',
                    'namn',
                ),
        )); ?>
    </div>
    
    <div class="col-md-4">
        <h2>Summary of Matches</h2>
        <?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
                'id'=>'stats-grid',
                'dataProvider'=>$arrayDataProvider,
                'type'=>'striped bordered',
                'hideHeader'=>true,
                'template'=>'{items}',
                'columns'=>array(
                    'id',
                    'value',
                ),
        )); ?>
    </div>
    
    <div class="col-md-4">
        <h2>Least Active Players</h2>
        <?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
                'id'=>'stats-grid',
                'dataProvider'=>$arrayDataProviderLeastActive,
                'type'=>'striped bordered',
                'hideHeader'=>true,
                'template'=>'{items}',
                'columns'=>array(
                    'id',
                    'totalPlayed',
                    'namn',
                ),
        )); ?>
    </div>
</div>

<div class="row">
    <?php 
        $dataPie = ''; // { label: 'Series 1', data: 3 },
        foreach ($topWinners as $match) {
            $dataPie[]=array(
                'label'=>'&nbsp;'. $match->namn . '&nbsp;('.$match->totalWon.')',
                'data'=>$match->totalWon*10
            );
        }
        $jsonDataPie=CJSON::encode($dataPie); 
    ?>
    <?php
    Yii::app()->clientScript->registerScript('topWinnersPie', "  
        /* Flot Pie Chart */
        $(function() {

            var data = $jsonDataPie;

            var plotObj = $.plot($('#flot-pie-chart'), data, {
                series: {
                    pie: {
                        show: true
                    }
                },
                grid: {
                    hoverable: true
                },
                tooltip: true,
                tooltipOpts: {
                    content: '%p.0%, %s', // show percentages, rounding to 2 decimal places
                    shifts: {
                        x: 20,
                        y: 0
                    },
                    defaultTheme: false
                }
            });

        });



    ");
    ?>
    
</div>