<?php 

error_reporting(E_ALL ^ E_DEPRECATED);
ini_set('display_errors','On');

set_include_path(get_include_path() . PATH_SEPARATOR . realpath("../includes/"));

require_once("configure.php"); // This is the function file that the that the system use.

$strMessage = "";


?>
<h1>Anv&auml;ndar</h1>

<h2><?php echo $strMessage;?></h2>

<table>
	<tr>
		<th>Anv&auml;ndarnamne</th>
		<th>Namn</th>
	</tr>
	<?php foreach (clsUser::getList("","","","") as $objUser):?>
		<tr>
			<td><a href="mailto:<?php echo $objUser->strUsername; ?>"><?php echo $objUser->strUsername; ?></a> </td>
			<td><?php echo $objUser->strFirstname . " " . $objUser->strLastname; ?> </td>
		 </tr>
	<?php endforeach;?>
</table>
